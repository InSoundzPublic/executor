#!/usr/bin/python3
import os
import sys
import requests
from setuptools import find_packages, setup

VERSION="1.6.1"

setup(
    name="executor",
    version=VERSION,
    author="InSoundz LTD",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["exec_model_cli=exec_model.exec_model:main"]
    },

    include_package_data=True,

    python_requires=">=3.8"
)
