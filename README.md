Executor
-----------------------
This file contains information specific on installing and running InSoundz Denoiser models.

Limitation
-----------------------
This (non-commercial) demo is limited to 20sec enhancing only. 

Pre-requisites MAC-OS (Intel CPU)
-----------------------
Homebrew - https://brew.sh/ <br>

```console
brew install sox
```


Pre-requisites Ubuntu
-----------------------
```console
apt install sox
```

Installation
-----------------------
```console
git clone https://gitlab.com/InSoundzPublic/executor.git
cd executor
./prepare 
```
Will download the model, and ask for microphone permission to allow the live execution of the models. <br>


```console
sudo pip3 install -r ./requirements.txt -U .
```
Installs the package. <br>


Live execution
-----------------------
This functionality allows the user to enhance data that is being recorded live using their devices' microphones.<br>

Live recording duration is restricted to 20 seconds. <br>

* Please note: before starting live execution, make sure that your microphone is not set to any external devices like headsets.

Run:

```console
./process <recording_name> mic
```

For example:
```console
./process test mic
```

Produces files:

| Description  | Path                    |
|--------------|:------------------------|
| input        |\<recording\_name\>\_input.wav
| output       | \<recording\_name\>\_output.wav
| normalized   |\<recording\_name\>\_normalized.wav 


Offline execution
-----------------------
This functionality allows the user to enhance existing data from a local file.<br>
Local file duration is restricted to 20 seconds. <br>


Run:

```console
./process <recording_name> <input_filename>
```

Produces files:

| Description  | Path                    |
|--------------|:------------------------|
| output       | \<recording\_name\>\_output.wav
| normalized   |\<recording\_name\>\_normalized.wav 


