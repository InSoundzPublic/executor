import io
import os
os.environ["CUDA_VISIBLE_DEVICE"]="-1"
import tensorflow.lite as lite
import argparse
from tqdm import tqdm
import numpy as np
import sys



SAMPLE_RATE=16000

def get_model_function(model_path, cores):
    def tflite(**kwargs):
        interpreter = lite.Interpreter(**kwargs)
        interpreter.allocate_tensors()
        model_runner = interpreter.get_signature_runner()
        signature = interpreter.get_signature_list()["serving_default"]

        def _runner(x):
            inputs ={signature["inputs"][0]:x} 
            res = model_runner(**inputs)
            return res[signature["outputs"][0]]

        return _runner

    with open(model_path, "rb") as f:
        model = f.read()

    return tflite(model_content=model, num_threads=cores)


def live_process(model, steplen, stream, gain, dtype):

    with tqdm(desc="Processing") as pbar:
        while True:
            pbar.update(1)
            data = stream.read(np.dtype(dtype).itemsize*steplen)
            data = np.frombuffer(data, dtype=dtype)
            data = data.astype("float32")

            if len(data) < steplen:
                break

            data = np.reshape(data, [1,1,1,steplen])
            data *= gain
            res = model(data)
            res /= gain
            res = res.astype(dtype)
            sys.stdout.buffer.write(res.tobytes())

def main():
    parser = argparse.ArgumentParser(description="Denoiser model evaluation")
    parser.add_argument("--steplen",type=int, default=1024)
    parser.add_argument("--cores", type=int, default=1)
    parser.add_argument("--input", default="-")
    parser.add_argument("--gain", type=float, default=1000.0)
    parser.add_argument("--dtype", default="float32")
    args = parser.parse_args()

    if args.input == "-":
        stream = sys.stdin.buffer
    else:
        import librosa
        assert args.dtype == "float32", "file loading only with float32"
        data, _ = librosa.load(args.input, SAMPLE_RATE, dtype="float32")
        stream = io.BytesIO(data.tobytes())

    model_path = os.path.join(os.path.dirname(__file__), "model.tflite")
    model = get_model_function(model_path, args.cores)
    
    live_process(model, args.steplen, stream, args.gain, args.dtype)


if __name__=="__main__":
    main()
